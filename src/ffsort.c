#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
 
int c;
int d;
int rectime;
int reclen;
int i;
int recordcount = 0;
int error;
int asterixcounter = 0;
int *prectime;
int *preclen;
unsigned char *errorcode;
unsigned char *line;
unsigned char *recordingday;
unsigned char *asterix;
unsigned char *tmpasterix;
FILE *f, *g;
char *inputfile = NULL, *outputfile = NULL;

void printusage(int mode) {
  if (mode == 0) {
    /* We print to standard output*/
    printf ("Usage: ffsort [ -i input file -o output file ]\n"
    "Sorts Final Format files\n"
    "  [ -h this help listing ]\n"
    "  [ -v prints the version ]\n");
  } else {
    /* We print to standard error*/
    fprintf (stderr,"Usage: ffsort [ -i input file -o output file ]\n"
    "Sorts Final Format files\n"
    "  [ -h this help listing ]\n"
    "  [ -v prints the version ]\n");
  }
}

void getreclen() {
  c = fgetc(f);
  if (c != -1 ) {
    reclen = c * 256;
    c = fgetc(f);
    reclen= c + reclen;
  } else {
    reclen = -1;
  }
}

void getrectime() {
  rectime = fgetc(f) * 65536;
  rectime = fgetc(f) * 256 + rectime;
  rectime = fgetc(f) + rectime;
}

int checktail() {
  int i;
  for (i = 0; i < 4; i++) {
    if (fgetc(f) != 165) {
      /* The tail should be a sequence of four 0xA5 (Decimal 165)
         or we return a -1 to signal an error */
      return -1;
    } 
  }
  /* The tail is fine, so we return a 0*/
  return 0;
}

int getrecordcount() {
  int i;
  /* We count the records and memorise them */
  while (1) {
    /* We entered an endless loop but it will terminate when
       c = -1 and getreclen returns a reclen = -1 */
    getreclen();
    preclen = realloc(preclen, (recordcount + 1) * sizeof(int));
    if (preclen == 0) {
      perror("realloc failed");
      abort();
    }
    *(preclen + recordcount) = reclen;
    if (reclen == -1) {
      /* We reached the End of File*/
      return recordcount;
    }
    errorcode = realloc(errorcode, (recordcount + 1) * sizeof(unsigned char));
    if (errorcode == 0) {
      perror("realloc failed");
      abort();
    }
    *(errorcode + recordcount) = fgetc(f);
    line = realloc(line, (recordcount + 1) * sizeof(unsigned char));
    if (line == 0) {
      perror("realloc failed");
      abort();
    }
    *(line + recordcount) = fgetc(f);
    recordingday = realloc(recordingday, (recordcount + 1) * sizeof(unsigned char));
    if (recordingday == 0) {
      perror("realloc failed");
      abort();
    }
    *(recordingday + recordcount) = fgetc(f);
    getrectime();
    prectime = realloc(prectime, (recordcount + 1) * sizeof(int));
    if (prectime == 0) {
      perror("realloc failed");
      abort();
    }
    *(prectime + recordcount) = rectime;
    /* We have 8 header octets
       and checktail reads the last 4 
       so we loop until reclen - 12 */
    for (i = 0; i < reclen - 12; i++) {
      asterixcounter++;
      asterix = realloc(asterix, asterixcounter * sizeof(unsigned char));
      if (asterix == 0) {
        perror("realloc failed");
        abort();
      }
      *(asterix + asterixcounter - 1) = fgetc(f);
    }
    error = checktail();
    if (error == -1) {
      /* The tail should be a sequence of four 0xA5 (Decimal 165)
         or we return a -1 to signal an error */
      return -1;
    }
    recordcount++;
  }
}

int sort() {
  int tmp;
  int i;
  int j;
  int k;
  int swap;
  /* We implement an optimsed bubble sorting algorithm here
     Feel free to improve it, if you are so inclined.*/
  for (i = 0; i < recordcount; i++) {
    asterixcounter = 0;
    swap = 0;
    for (j = 0; j < recordcount - i - 1; j++) {
      if (*(prectime + j) > *(prectime + j + 1)) {
        swap = 1;
        tmp = *(prectime + j);
        *(prectime + j) = *(prectime + j + 1);
        *(prectime + j + 1) = tmp;
        /* We are in record j, for *asterix
           the initial pointer for record "j" is "asterixcounter"
           the initial pointer for record "j + 1" is "asterixcounter + *(preclen + j) - 12"

           Store a temporary copy of asterix of record j in tmpasterix */
        tmpasterix = realloc(tmpasterix, (*(preclen + j) - 12) * sizeof(unsigned char));
        if (tmpasterix == 0) {
          perror("realloc failed");
           abort();
        }
        for (k = 0; k < *(preclen + j) - 12 ; k++) {
          *(tmpasterix + k) = *(asterix + asterixcounter + k);
        }

        /* Copy asterix of record j + 1 on the asterix of record j */
        for (k = 0; k < *(preclen + j + 1) - 12; k++) {
          *(asterix + asterixcounter + k) = *(asterix + asterixcounter + *(preclen + j) - 12 + k);
        }

        /* Paste the temporary storage of asterix of record (j) (tmpasterix)
           after the asterix of record (j + 1) that we had just copied on the
           intitial pointer of record (j) */
        for (k = 0; k < *(preclen + j) - 12; k++) {
          *(asterix + asterixcounter + *(preclen + j + 1) - 12 + k) = *(tmpasterix + k);
        }

        tmp = *(preclen + j);
        *(preclen + j) = *(preclen + j + 1);
        *(preclen + j + 1) = tmp;
        tmp = *(errorcode + j);
        *(errorcode + j) = *(errorcode + j + 1);
        *(errorcode + j + 1) = tmp;
        tmp = *(line + j);
        *(line + j) = *(line + j + 1);
        *(line + j + 1) = tmp;
      }
      asterixcounter = asterixcounter + *(preclen + j) - 12;
    }
    if (swap == 0) {
      return 0;
    }
  }
  return 0;
}

void printdump() {
  int i;
  int j;
  int k = 0;
  for (i = 0; i < recordcount; i++) {
    fprintf (g, "%c", *(preclen + i) / 256);
    fprintf (g, "%c", *(preclen + i) % 256);
    fprintf (g, "%c", *(errorcode + i));
    fprintf (g, "%c", *(line + i));
    fprintf (g, "%c", *(recordingday + i));
    fprintf (g, "%c", *(prectime + i) / 65536);
    fprintf (g, "%c", (*(prectime + i) % 65536) / 256);
    fprintf (g, "%c", *(prectime + i) % 256);
    for (j=0 ; j < *(preclen + i) - 12; j++) {
      fprintf (g, "%c", *(asterix + k));
      k++;
    }
    fprintf (g, "%c", 165);
    fprintf (g, "%c", 165);
    fprintf (g, "%c", 165);
    fprintf (g, "%c", 165);
  }
}

int main (int argc, char *argv[])
{
  while (1)
  {
  static struct option long_options[] =
    {
    /* These options don’t set a flag.
       We distinguish them by their indices. */
      {"version",     no_argument,       0, 'v'},
      {"help",  no_argument,       0, 'h'},
      {"inputfile",  required_argument, 0, 'i'},
      {"outputfile",  required_argument, 0, 'o'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;
    d = getopt_long (argc, argv, "vhi:o:",
                       long_options, &option_index);
    /* Detect the end of the options. */
    if (d == -1) {
      break;
    }
    switch (d)
    {
      case 'v':
        printf ("ffsort version 1.0\n");
        return 0;
        break;
      case 'h':
        printusage(0);
        return 0;
        break;
      case 'i':
        inputfile = optarg;
        f = fopen(inputfile, "r");
        if (f == 0) {
          fprintf (stderr, "Unable to open input file %s\n", inputfile);
          return 1;
        }
        break;
      case 'o':
        outputfile = optarg;
        g = fopen(outputfile, "w");
        if (g == 0) {
          fprintf (stderr, "Unable to open output file %s\n", outputfile);
          return 1;
        }
        break;
      default:
        fprintf(stderr, "Option is incorrect\n");
        return 1;
    }
  }
  if (f == 0) {
    fprintf (stderr, "No input file selected\n\n");
    printusage(1);
    return 1;
  }
  if (g == 0) {
    fprintf (stderr, "No output file selected\n\n");
    printusage(1);
    return 1;
  }

  recordcount = getrecordcount();
  if (recordcount == -1) {
    fprintf(stderr, "error, incorrect input file format\n\n");
    printusage(1);
    return 1;
  }
  sort();
  printdump();
  free(preclen);
  free(errorcode);
  free(recordingday);
  free(tmpasterix);
  free(asterix);
  fclose(f);
  fclose(g);
  return 0;
}
