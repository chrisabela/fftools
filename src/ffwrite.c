#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
 
int d;
int c;
int column = 0;
int freshline = 0;
int filelineno = 1;
int lineno;
int lineno;
int errorcode;
int rectime;
int tmpdec;
int datalength;
int parityflag;
FILE *f;
FILE *g;
char *inputfile = NULL;
char *outputfile = NULL;
unsigned char *asterix;

void printusage(int mode) {
  if (mode == 0) {
    /* We print to standard output*/
    printf ("Usage: ffwrite [ -i input file -o output file ]\n"
    "Writes Final Format files from text files\n"
    "             [ -h this help listing ]\n"
    "             [ -v prints the version ]\n");
  } else {
    /* We print to standard error*/
    fprintf (stderr,"Usage: ffwrite [ -i input file -o output file ]\n"
    "Writes Final Format files from text files\n"
    "             [ -h this help listing ]\n"
    "             [ -v prints the version ]\n");
  }
}

void ascii2dec() {
  if ( c >= 48 && c <= 59) {
    /* We expect 0 - 9 only here*/
    tmpdec = c - 48;
  } else {
    tmpdec = -1;
  }
}

int asciihex2dec() {
  int decimal;
  /* We expect 0-9 or A-F here */
  if ( c >= 48 && c <= 59) {
    /* 0 - 9 only here*/
    decimal = c - 48;
  } else if (c >= 65 && c <= 70) {
    /* We expect 0xA to 0xF only here, 0xA = 10 and 0xB = 11 and so on */
    decimal = c - 55;
  } else {
    decimal = -241;
    /* If the first digit is 0xF (Decimal: 15), it would raise the value 
       to 15 * 16 = 240.
       The second digit comes along and it may be out of range.
       So we ensure that we return a negative number by decreasing it
       by one more than its maximum value:
       240 - 241 = -1*/
  }
  return decimal;
}

int printdump() {
  int i;
  if (parityflag !=0 ) {
    fprintf (stderr, "Error: an odd number of half-octets were found in line %d\n\n", filelineno);
    return 1;
  }
  /* Final Format has a header of 8 bytes and a tail of 4 bytes*/
  fprintf (g, "%c", (datalength + 12) / 256);
  fprintf (g, "%c", (datalength + 12) % 256);
  fprintf (g, "%c", errorcode);
  fprintf (g, "%c", lineno);
  /* We will assume that the recording day is 0*/
  fprintf (g, "%c", 0);
  fprintf (g, "%c", rectime/65536);
  fprintf (g, "%c", rectime/256);
  fprintf (g, "%c", rectime%256);
  for (i = 0 ; i < datalength; i++) {
    fprintf (g, "%c", *(asterix + i));
  }
  /* Next comes the tail, a sequence of four 0xA5*/
  fprintf (g, "%c", 165);
  fprintf (g, "%c", 165);
  fprintf (g, "%c", 165);
  fprintf (g, "%c", 165);
  return 0;
}

int main (int argc, char *argv[])
{
  int payload;
  while (1)
  {
  static struct option long_options[] =
    {
    /* These options don’t set a flag.
       We distinguish them by their indices. */
      {"version",     no_argument,       0, 'v'},
      {"help",  no_argument,       0, 'h'},
      {"inputfile",  required_argument, 0, 'i'},
      {"outputfile",  required_argument, 0, 'o'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;
    d = getopt_long (argc, argv, "vhi:o:",
                       long_options, &option_index);
    /* Detect the end of the options. */
    if (d == -1) {
      break;
    }
    switch (d)
    {
      case 'v':
        printf ("ffwrite version 1.0\n");
        return 0;
        break;
      case 'h':
        printusage(0);
        return 0;
        break;
      case 'i':
        inputfile = optarg;
        f = fopen(inputfile, "r");
        if (f == 0) {
          fprintf (stderr, "Unable to open input file %s\n", inputfile);
          return 1;
        }
        break;
      case 'o':
        outputfile = optarg;
        g = fopen(outputfile, "w");
        if (g == 0) {
          fprintf (stderr, "Unable to open output file %s\n", outputfile);
          return 1;
        }
        break;
      default:
        fprintf(stderr, "Option is incorrect\n");
        return 1;
    }
  }
  if (f == 0) {
    fprintf (stderr, "No input file selected \n\n");
    printusage(1);
    return 1;
  }
  if (g == 0) {
    fprintf (stderr, "No output file selected \n\n");
    printusage(1);
    return 1;
  }
  while ( c != -1 ) {
    column++;
    c = fgetc (f);
    switch (c) {
      case 10:
        if (datalength != 0) {
          if (printdump() == 1) {
            return 1;
          }
          /* Set the parityflag*/
          parityflag = 2;
        }
        if (column > 1 && column < 24) {
          fprintf (stderr, "Error: Unexpected end of line %d\n\n", filelineno);
          return 1;
        }
        freshline = 1;
        column = 0;
        datalength = 0;
        filelineno++;
        break;
      case 13:
        /* DOS formatted input file */
        if (column > 1 && column < 24) {
          fprintf (stderr, "Error: Unexpected end of line %d\n\n", filelineno);
          return 1;
        }
        column = 0;
        break;
      default:
        freshline = 0;
    }
    if ( freshline == 0 && c != -1 ) {
    switch (column) {
      case 1:
        /* We expect to find character "L" here or we throw a tantrum */
        if ( c != 76 ) {
          fprintf (stderr, "Error: L is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
          }
        break;
      case 2:
        lineno = asciihex2dec() * 16;
        if (lineno < 0 ) {
          fprintf (stderr, "Error: Line no is out of the expected range of 00 - FF, in line %d\n\n", filelineno);
          return 1;
        }
        break;
      case 3:
        lineno = asciihex2dec() + lineno;
        if (lineno < 0  ) {
          fprintf (stderr, "Error: Line no is out of the expected range of 00 - FF, in line %d\n\n", filelineno);
          return 1;
        }
        break;
      case 5:
        /* We expect an 'E' here */
        if (c != 69) {
          fprintf (stderr, "Error: E is expected in the column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        break;
      case 6:
        ascii2dec();
        errorcode = 10 * tmpdec;
        if (errorcode < 0 ) {
          fprintf (stderr, "Error: a digit (0-9) is is expected in the column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        break;
      case 7:
        ascii2dec ();
        errorcode = errorcode + tmpdec;
        if (errorcode < 0 ) {
          fprintf (stderr, "Error: a digit (0-9) is is expected in the column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        break;
      case 9:
        /* We expect a "T" in column 9 */
        if (c != 84) {
          fprintf (stderr, "Error: a T is is expected in the column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        break;
      case 10:
        /* We expect a digit here
           denoting tens of hours (0 - 2)
           10*60*60*100 = 3600000 centiseconds  */
        ascii2dec();
        if (tmpdec < 0 || tmpdec > 2) {
          fprintf (stderr, "Error: a digit (0 - 2) is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        rectime = 3600000 * tmpdec;
        break;
      case 11:
        /* We expect a digit here
           denoting hours (00 - 23)
           60*60*100 = 360000 centiseconds  */
        ascii2dec();
        rectime = rectime + 360000 * tmpdec;
        if (tmpdec < 0) {
          fprintf (stderr, "Error: a digit (0-9) is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        /* 23 hours = 23 * 60 * 60* 100 centiseconds
           = 8280000 */
        if (rectime > 8280000) {
          fprintf (stderr, "Error: Time is out of range in column %d, line %d\n\n", column, filelineno);
          return 1;
        }
        break;
      case 12:
        /* We expect a : here */
        if (c != 58) {
          fprintf (stderr, "Error: \":\" is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        break;
      case 13:
        /* We expect a digit here
           denoting tens of minutes (0 - 5)
           10 * 60 * 100 = 60000 centiseconds */
       ascii2dec();
        if (tmpdec < 0) {
          fprintf (stderr, "Error: a digit (0-9) is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        rectime = rectime + 60000 * tmpdec;
        if (tmpdec > 5) {
          fprintf (stderr, "Error: Time is out of range in column %d, line %d\n\n", column, filelineno);
          return 1;
        } 
        break;
      case 14:
        /* We expect a digit here
           denoting minutes (0 - 9)
           60 * 100 = 6000 centiseconds */
       ascii2dec();
        if (tmpdec < 0) {
          fprintf (stderr, "Error: a digit (0-9) is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        rectime = rectime + 6000 * tmpdec;
        break;
      case 15:
        /* We expect a : here */
        if (c != 58) {
          fprintf (stderr, "Error: \":\" is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        break;
      case 16:
        /* We expect a digit here
           denoting tens of seconds (0 - 9)
           10 * 100 = 1000 centiseconds */
       ascii2dec();
        if (tmpdec < 0) {
          fprintf (stderr, "Error: a digit (0-9) is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        rectime = rectime + 1000 * tmpdec;
        break;
      case 17:
        /* We expect a digit here
           denoting seconds (0 - 9) 
           or 100 centiseconds*/
       ascii2dec();
        if (tmpdec < 0) {
          fprintf (stderr, "Error: a digit (0-9) is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        rectime = rectime + 100 * tmpdec;
        break;
      case 18:
        /* We expect a , here */
        if (c != 44) {
          fprintf (stderr, "Error: \",\" is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        break;
      case 19:
        /* We expect a digit here
           denoting tens of centiseconds (0 - 9) 
           or 10 centiseconds*/
       ascii2dec();
        if (tmpdec < 0) {
          fprintf (stderr, "Error: a digit (0-9) is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        rectime = rectime + 10 * tmpdec;
        break;
      case 20:
        /* We expect a digit here
           denoting centiseconds (0 - 9) */
       ascii2dec();
        if (tmpdec < 0) {
          fprintf (stderr, "Error: a digit (0-9) is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        rectime = rectime + tmpdec;
        break;
      case 22:
        /* We expect a - here */
        if (c != 45) {
          fprintf (stderr, "Error: \"-\" is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        break;
      case 23:
        /* We expect a > here */
        if (c != 62) {
          fprintf (stderr, "Error: \">\" is is expected in column %d but %c found in line %d\n\n", column, c, filelineno);
          return 1;
        }
        break;
      }
    }
    /* We expect a white space here (Decimal 32)
       Perhaps we should not stop but we should send a warning, if this is not what we get */
    if (column == 4 || column == 8 || column == 21) {
      if (c != 32 ) {
        fprintf (stderr, "Warning: white space expected in column %d in line %d\n\n", column, filelineno);
      }
    }
    if (column > 23) {
      /* Check for Alphanumeric */
      if (c != 32) {
        /* We exclude white spaces */
        payload = asciihex2dec();
        if (payload >= 0) {
          /* parityflag takes a value of  1 or 0
             the first payoad has a parityflag of 1
             the second payload has parityflag of 0    
          */
          parityflag--;
          if (parityflag == -1) {
            parityflag = 1;
          }
        switch (parityflag) {
          case 1:
            datalength++;
            tmpdec = payload * 16;
            break;
          case 0:
            asterix = realloc (asterix, sizeof(int) * datalength);
            if (asterix == 0) {
              perror ("realloc failed");
              abort ();
            }
            *(asterix + datalength - 1) = (payload + tmpdec);
            break;
          }
        } else {
          fprintf (stderr, "Error: Unexpected character (%c) found in column %d in line %d\n\n", c, column, filelineno);
          return 1;
        }
      }
    }
  }
  fclose (f);
  fclose (g);
  free (asterix);
  return 0;
}
