#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
 
int d;
int sourceline = -1;
int destinationline = -1;
int reclen;
int c;
int lineno;
int mmsb;
int msb;
int lsb;
char *inputfile = NULL;
char *outputfile = NULL;
FILE *f;
FILE *g;

void printusage(int mode) {
  if (mode == 0) {
    /* We print to standard output*/
    printf ("Usage: fflinealt [ -i input file -o output file -s source line number -d destination line number ]\n"
    "Alters a selected Line Number of a Final Format file\n"
    "             [ -h this help listing ]\n"
    "             [ -v prints the version ]\n");
  } else {
    /* We print to standard error*/
    fprintf (stderr,"Usage: fflinealt [ -i input file -o output file -s source line number -d destination line number ]\n"
    "Alters a selected Line Number of a Final Format file\n"
    "             [ -h this help listing ]\n"
    "             [ -v prints the version ]\n");
  }
}

int getlineno() {
int i;
int tmp = 0;
int line = 0;
  for (i = 0; optarg[i] != '\0'; i++) {
    if (optarg[i] > 47 && optarg[i] < 58) {
       /* 0 - 9 digit */
       line = optarg[i] - 48;
    } else if (optarg[i] > 64 && optarg[i] < 71 ) {
      /* A - F*/
      line = optarg[i] - 55;
    } else if (optarg[i] > 96 && optarg[i] < 103) {
      /* a - f*/
      line = optarg[i] - 87;
    } else return -1;
    line = tmp * 16 + line;
    tmp = line;
  }
  return line;
}

int getreclen() {
  int tmp = 0;
  c = fgetc(f);
  if (c != -1) {
    fprintf(g, "%c", c);
    tmp = c;
    c = fgetc(f);
  } else /* Expected End of File */ return 0;
  if (c != - 1) {
    fprintf(g, "%c", c);
    reclen = 256 * tmp + c;
  } else reclen = -1;
  /* An unepected End of File returns -1 */
  return reclen;
}

int printdump() {
  int i;
  while (1) {
    /* get record length */
    reclen = getreclen();
    if (reclen == -1) {
      return -1;
    }
    if (reclen == 0) {
      return 0;
    }
    /* get errorcode */
    c = fgetc(f);
    if (c == -1) {
      return -1;
    }
    fprintf(g, "%c", c);
    /* get line number */
    c = fgetc(f);
    if (c == -1) {
      return -1;
    }
    if (c == sourceline) {
      c = destinationline;
    }
    fprintf(g, "%c", c);
    /* get recording day, time mmsb, time msb, time lsb and ASTERIX payload */
    for (i =0; i < reclen - 8; i++) {
      c = fgetc(f);
      if (c == -1) {
        return -1;
      }
      fprintf(g, "%c", c);
    }
    /* get padding of 4 octets of 0xA5 */
   for (i = 0; i < 4; i++) {
     c = fgetc(f);
     if (c != 165) {
       return -1;
     }
   fprintf(g, "%c", 165);
   }
  }
  return 0;
}

int main (int argc, char *argv[])
{
  int errorcode;
  while (1)
  {
  static struct option long_options[] =
    {
    /* These options don’t set a flag.
       We distinguish them by their indices. */
      {"version",     no_argument,       0, 'v'},
      {"help",  no_argument,       0, 'h'},
      {"inputfile",  required_argument, 0, 'i'},
      {"outputfile",  required_argument, 0, 'o'},
      {"source",  required_argument, 0, 's'},
      {"destination",  required_argument, 0, 'd'},
      {0, 0, 0, 0}
    };
    /* getopt_long stores the option index here. */
    int option_index = 0;
    d = getopt_long (argc, argv, "vhi:o:s:d:",
                       long_options, &option_index);
    /* Detect the end of the options. */
    if (d == -1) {
      break;
    }
    switch (d)
    {
      case 'v':
        printf ("fflinealt version 1.0\n");
        return 0;
        break;
      case 'h':
        printusage(0);
        return 0;
        break;
      case 'i':
        inputfile = optarg;
        f = fopen(inputfile, "r");
        if (f == 0) {
          fprintf (stderr, "Unable to open input file %s\n", inputfile);
          return 1;
        }
        break;
      case 'o':
        outputfile = optarg;
        g = fopen(outputfile, "w");
        if (g == 0) {
          fprintf (stderr, "Unable to open output file %s\n", outputfile);
          return 1;
        }
        break;
      case 's':
        sourceline = getlineno();
        if (sourceline == -1 || sourceline > 255) {
          fprintf (stderr, "The source line has to be in alphanmeric format (0 - FF)\n");
          return 1;
        }
        break;
      case 'd':
        destinationline = getlineno();
        if (destinationline == -1 || destinationline > 255) {
          fprintf (stderr, "The destination line has to be in alphanmeric format (0 - FF) \n");
          return 1;
        }
        break;
      default:
        fprintf(stderr, "Option is incorrect\n");
        return 1;
    }
  }
  if (f == 0) {
    fprintf (stderr, "No input file selected \n\n");
    printusage(1);
    return 1;
  }
  if (g == 0) {
    fprintf (stderr, "No output file selected \n\n");
    printusage(1);
    return 1;
  }
  if (sourceline == -1) {
    fprintf (stderr, "A source line number has to be specified\n\n");
    printusage (1);
    return 1;
  }
  if (destinationline == -1) {
    fprintf (stderr, "A destination line number has to be specified\n\n");
    printusage (1);
    return 1;
  }
  errorcode = printdump();
  if (errorcode ==  -1) {
    fprintf (stderr, "error: the input file is corrupted\n\n");
    printusage (1);
    return 1;
  }
  return 0;
}
